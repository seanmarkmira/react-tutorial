export const books = [
  {
    id: 1,
    img: "https://images-na.ssl-images-amazon.com/images/I/91-EIJiYneL._AC_UL604_SR604,400_.jpg",
    title: "Atomic Habits",
    author: "James Clear",
  },

  {
    id: 2,
    img: "https://images-na.ssl-images-amazon.com/images/I/81s0B6NYXML._AC_UL604_SR604,400_.jpg",
    title: "It Ends with Us: A Novel",
    author: "Colleen Hoover",
  },
  {
    id: 3,
    img: "https://images-na.ssl-images-amazon.com/images/I/91-EIJiYneL._AC_UL604_SR604,400_.jpg",
    title: "Atomic Habits",
    author: "James Clear",
  },
];
