/*
Very important concepts:
  1. When you pass an arguement in React for example 
        complexExample(someArguement)
     What happens here is that once the page loads it will be called despite the function must be triggered by an event since you are invoking the function. If you need to pass an arguement what you need to do is to wrap the complexExample(someArguement) into a function.
        onClick{() => complexExample(someArguement)}
*/
// Importing CSS file
import "./index.css";

//You can skip import React from 'react'
import ReactDom from "react-dom";
import { books } from "./books";
import Book from "./Book";
const title = "Atomic Habits";
const author = "James Clear";
const image =
  "https://images-na.ssl-images-amazon.com/images/I/91-EIJiYneL._AC_UL604_SR604,400_.jpg";

function BookList() {
  return (
    <section className="booklist">
      {books.map((book) => {
        const { img, title, author } = book;
        return <Book {...book} key={book.id}></Book>; //{...book} is another way to pass something to the function. With this, no need to destructure the object
      })}
    </section>
  );
}

//What this code means is that we render the Greeting function and inject to dom:'root'
ReactDom.render(<BookList />, document.getElementById("root"));

// The block of code below shows the concept of children in a component. This children is passed and can be called in the other function.
// function BookList() {
//   return (
//     <section className="booklist">
//       {/* This is like calling the function Book and we can pass a value to it */}
//       <Book
//         img={firstBook.img}
//         title={firstBook.title}
//         author={firstBook.author}
//       >
//         {/* This is a children of the component which can be accessed through a special key which is children. See Book function */}
//         <p>
//           lorem123 lorem123 lorem123 lorem123lorem123 lorem123 lorem123 lorem123
//           lorem123
//         </p>
//       </Book>
//
//       <Book
//         img={secondBook.img}
//         title={secondBook.title}
//         author={secondBook.author}
//       />
//       <Book />
//     </section>
//   );
// }
