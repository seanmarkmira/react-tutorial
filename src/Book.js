const Book = ({ img, title, author }) => {
  // children is a special parameter where in the call in the BookList, anything between the closing and opening tag will be put into the 'children' props

  //To make an event in React you need: attribute (onClick, onMouseOver, etc.), and eventHandler. You can implement this in two ways: in line function or function call. See clickHandler and button below:

  const clickHandler = (e) => {
    console.log(e);
    console.log(e.target);
    alert("Hello World");
  };

  const complexExample = (author) => {
    console.log(author);
  };

  return (
    <article
      className="book"
      onMouseOver={() => {
        console.log("mouse hovers");
      }}
    >
      <img
        src={img}
        alt="This is a book"
        width="300"
        height="200"
        onMouseOver={() => {
          console.log("Mouse on Image");
        }}
      />
      <h1 onClick={() => console.log(title)}>{title}</h1>
      <h4>{author}</h4>
      <button type="button" onClick={clickHandler}>
        Sample
      </button>
      <button type="button" onClick={() => complexExample(author)}>
        more complex example
      </button>
    </article>
  );
};

//You can only have one export per file
export default Book;
